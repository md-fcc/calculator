class Calculator {
	// Calculator class computes the result of an input mathematical expression.
	// The input is an array of operators and operands in prefix notation.

	constructor() {
		// this.operators contains the math functions that the calculator can perform
		// precedence: defines which operators should be handled first 
		//             (larger precedence handled first)
		// associative: defines if the operator applies to the right or left operand
		//              ex. a ^ b  "^" operates to the right using b
		// execute: does the actual math
		
		this.operators = {
			"-u": {  // unary minus for negative numbers
				precedence: 5,  // must take highest precedence to distinguish from binary minus
				associativity: "Right",
				execute: (a) => {return -1 * a;}
			},
			"^": {
				precedence: 4,
				associativity: "Right",
				execute: (a, b) => {return Math.pow(a, b);}
			},
			"*": {
				precedence: 3,
				associativity: "Left",
				execute: (a, b) => {return a * b;}
			},
			"/": {
				precedence: 3,
				associativity: "Left",
				execute: (a, b) => {return a / b;}
			},
			"+": {
				precedence: 2,
				associativity: "Left",
				execute: (a, b) => {return a + b;}
			},
			"-": {  // binary minus, subtract two numbers
				precedence: 2,
				associativity: "Left",
				execute: (a, b) => {return a - b;}
			}
		}
	};
	
	
	isNumeric(string) {
		// returns true if the input string represents a number
		return !isNaN(parseFloat(string)) && isFinite(string);
	};


	prefixToPostfix (inputStack) {
		// converts an array (stack) of operands and operators, written in prefix notation,
		// to an array of operands and operators written in postfix notation
		
		// Shunting-yard algorithm ---------------------------------------------
		// inspired by code from: https://www.thepolyglotdeveloper.com/2015/03/parse-with-the-shunting-yard-algorithm-using-javascript/
        // modified this code to use "modern" features: filter function, arrow function, "let" instead of "var", ...
        // modified to use "defensive programmining" if...continue instead of if...else if...
        // modified to follow more closely the psuedocode on https://en.wikipedia.org/wiki/Shunting-yard_algorithm
		
		let operatorStack = [],
			postfixStack = [],
			lastToken = "";
	
		for(let i = 0; i < inputStack.length; i++) {
			let token = inputStack[i];
			
			// expand "-" token to "-u" for unary minus
			if (token == '-' && (this.operators.hasOwnProperty(lastToken) || i == 0)) {
				token = '-u';
			}
			
			
			if (this.isNumeric(token)) {
				postfixStack.push(token);
				lastToken = token;
				continue;
			}
			

			if (this.operators.hasOwnProperty(token)) {
								
				let topOpStackToken = operatorStack[operatorStack.length - 1];

				while
					(this.operators.hasOwnProperty(topOpStackToken) && 
					((this.operators[token].associativity === "Left" && 
						this.operators[token].precedence <= this.operators[topOpStackToken].precedence)
					|| (this.operators[token].associativity === "Right" && 
							this.operators[token].precedence < this.operators[topOpStackToken].precedence)
					))
				{
					postfixStack.push(operatorStack.pop());
					topOpStackToken = operatorStack[operatorStack.length - 1];
				}
				
				operatorStack.push(token);
				lastToken = token;
				continue;
			}
			

			if(token === "(") {
				operatorStack.push(token);
				lastToken = token;
				continue;
			}
			
			
			if(token === ")") {
				while(operatorStack[operatorStack.length - 1] !== "(") {
					postfixStack.push(operatorStack.pop());
				}
				// TODO: handle error that matching parenthesis not found!
				operatorStack.pop();
				lastToken = token;
				continue;
			}
		}

		while(operatorStack.length > 0) {
			postfixStack.push(operatorStack.pop());
		}

		return postfixStack;
	};
	
	
	evalPostfixStack(postfixStack) {
		// takes an array of operands and operators ordered in postfix fashion
		// and evaluates the answer
		
		// left-to-right postfix evaluation algorithm ----------------
		// https://en.wikipedia.org/wiki/Reverse_Polish_notation
		let answerStack = [];
	
		for (let token of postfixStack) {
			if (this.operators.hasOwnProperty(token)) {
				

				// pop off required number of operands for the function being executed
				let operands = [];
				
				for (let i = 0; i < this.operators[token].execute.length; i++) {
					operands.push(parseFloat(answerStack.pop()));
				}

				operands.reverse(); // postfix notation lists arguments backwards

				let result = this.operators[token].execute.apply(null, operands);
								
				answerStack.push(result);
				continue;
			}
			
			if (this.isNumeric(token)) {
				answerStack.push(token);
			}
		}

		return answerStack.pop();
	};


	eval(inputPrefixExpression) {
		// returns the resulting answer of an input string of operands and operators
		// written in prefix notation
		
		let postfixExpression = this.prefixToPostfix(inputPrefixExpression);
		return this.evalPostfixStack(postfixExpression);
	}
	
}

// -----------------------------------------------------------------------------
// Closure defining rounding operations for displaying results
// Allows results from the calculator to be 'intuitive', e.x.:
// 0.1 + 0.2 = 0.3, not shown as 0.30000000000000004
// Code from:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round

(function() {
  /**
   * Decimal adjustment of a number.
   *
   * @param {String}  type  The type of adjustment.
   * @param {Number}  value The number.
   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
   * @returns {Number} The adjusted value.
   */
  function decimalAdjust(type, value, exp) {

    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
	
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (value === null || isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }

    if (value < 0) {
      return -decimalAdjust(type, -value, exp);
    }
	
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    
	// Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }
})();
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// --------------------------- MAIN --------------------------------------------
// -----------------------------------------------------------------------------
let calc = new Calculator;

 // Stores an array of the numbers and inputs
let expressionStack = ["0"];

// justComputed is a boolean to indicate if the equals button was just pressed.
// Set to false anytime the results display updates.
// Allows the user to start over or continue from the previous answer.
//
// examples:
//     answer = 5, justComputed = true, press 7, sets expression stack to ["7"], update display, justComputed = false
//     answer = 5, justComputed = true, press +, sets expression stack to ["5", "+"], update display, justComputed = false
//
let justComputed = true; 

// -----------------------------------------------------------------------------
// --------------------- Helper functions --------------------------------------
// -----------------------------------------------------------------------------

setCalcToEmptyState = function() {
	expressionStack = ["0"];
	updateResultsDisplay(expressionStack);
	justComputed = true; // allows displayed zero to be overwritten with next number
}


updateResultsDisplay = function(newText) {
	// updates the HTML with newText. 
	// newText can be an array or string.  If newText is an array, the array is 
	// collapsed to a string with no delimeters using array.join("")
	
	let updatedText = newText;
	if (newText.constructor === Array) {
		updatedText = newText.join("");
	}

	$("#expression-view").html(updatedText);
	
	justComputed = false; // most common case is that this function has run after a user input

	// handle scrollbars
	if ($("#expression-view").width() > $("#expression-view-scroller").width()) {
		// expand the wrapper width + a few extra pixels so that the text 
		// does not get cut-off by the "#expression-view-scroller" border
		$("#expression-view-wrapper").width($("#expression-view").width() + 10);
	} else {
		$("#expression-view-wrapper").width($("#expression-view-scroller").width());	
	}
	$("#expression-view-scroller").scrollLeft($("#expression-view").width());
}


pushToInputStack = function(num) {
	// pushes num onto the expression stack
	// e.x. ["42", "+"], num = 7, mutate stack to ["42", "+", "7"]
	expressionStack.push(num.toString());
	updateResultsDisplay(expressionStack);
}


concatToInputStack = function(num) {
	// concatenates num to the last element in the expressionStack
	// e.x. ["42", "+", "7"], num = 9, mutate stack to ["42", "+", "79"]
	let prevNum = expressionStack.pop();
	let newNum = prevNum.toString() + num.toString();
	pushToInputStack(newNum);
}

                          
getLastToken = function(expressionStack) {
	// returns last token on the input stack. e.x. ["42", "+", "128"], returns "8"
	let lastToken = ""
	if (expressionStack.length > 0) {
		lastToken = expressionStack[expressionStack.length - 1].slice(-1);
	}
	return lastToken;
}


// -----------------------------------------------------------------------------
// --------------------- functions for HTML buttons ----------------------------
// -----------------------------------------------------------------------------


inputNumber = function(num) {
	// Puts a number on the input stack, either concatenating to an existing number,
	// or starting a new number.
	
	let lastToken = getLastToken(expressionStack);
	
	if ((calc.isNumeric(lastToken) || lastToken == ".") && !justComputed) {
		concatToInputStack(num);
		return;
	} else if (justComputed) {
		// expressionStack held answer, now want to overwrite with new num
		expressionStack = [];
	}

	pushToInputStack(num);
}


inputOperator = function(op) {
	// Handles adding an operator to the end of the current expression.
	// Prevents adding more than one consecutive operator.

	if (expressionStack.length == 0) {
		// assume cannot start expression with an operator
		// would need to change this for complex operators, like sin, cos, ...
		return;
	}
    	
	let lastElement = expressionStack[expressionStack.length - 1];

	if (lastElement == op) {
		// do not allow same operator to be input more than once, ex. "+++"
		return
	}

	if (calc.operators.hasOwnProperty(lastElement)) {
		expressionStack.pop(); // pop so that new op overwrites previous op
	} 
	pushToInputStack(op);
}


inputDot = function() {
	// handles input of a dot "." Prevents multiple dots "..." or "3.2.1"
	let lastToken = ""

	if (justComputed) {
		expressionStack = [];
	} else {
		lastToken = getLastToken(expressionStack);
	}

	if (!lastToken || calc.operators.hasOwnProperty(lastToken) || lastToken == "(") {
		let num = "0.";
		pushToInputStack(num);
		return;
	}

	let lastElement = expressionStack[expressionStack.length - 1];
	if (lastToken == "." || (lastElement.indexOf(".") !== -1)) {
		return;
	}

	concatToInputStack(".");
}


inputNegate = function() {
	// Switches number from positive to negative and vise versa.

	let lastElement = expressionStack[expressionStack.length - 1];
	if (calc.isNumeric(lastElement)) {
		lastElement = expressionStack.pop();
		if (lastElement.slice(0, 1) == "-") {
			expressionStack.push(lastElement.slice(1, lastElement.length))
		} else {
			expressionStack.push("-" + lastElement)
		}
	}
	
	updateResultsDisplay(expressionStack);
}


inputParentheses = function(paren) {
	// handles left and right parentheses input
	if (paren == "(") {
		let lastToken = getLastToken(expressionStack);

		if (calc.isNumeric(lastToken)) {
			// implicit multiply if add parenthesis after a number 6(7) = 6 * (7)
			expressionStack.push("*");	
		}
	}
	
	pushToInputStack(paren);
}


inputBackspace = function() {
	// Removes last token and padding from the current expression

	if (expressionStack.length == 0) {
		return;
	}

	let lastElement = expressionStack[expressionStack.length - 1];
	if (calc.isNumeric(lastElement) && lastElement.length > 1) {
		lastElement = expressionStack.pop();
		expressionStack.push(lastElement.slice(0, lastElement.length - 1));
	} else {
		expressionStack.pop();
	}

	if (expressionStack.length == 0) {
		setCalcToEmptyState()
	} else {
		updateResultsDisplay(expressionStack);
	}
};


inputAllClear = function() {
	// clears the entire current expression
	setCalcToEmptyState()
};


inputEraseHistory = function() {
	$("#expression-history").html("Calculation History");
}


inputEquals = function() {
    if (expressionStack.length == 0) {
        expressionStack = ["0"];
    }
    
    let precision = -10;  // number of decimal places
    let answer = Math.round10(calc.eval(expressionStack), precision);
    let expressionString = expressionStack.join("");
    
    expressionStack = [];
    
    if (!calc.isNumeric(answer)) {
        answer = "Error";
        expressionStack = ["0"];
    } else {
        expressionStack.push(answer.toString());
    }
    
    updateResultsDisplay(answer);
    $("#expression-view-scroller").scrollLeft(0);
    let newHistoryItem = expressionString + " = " + answer
    $("#expression-history").html($("#expression-history").html() + "<br>" + newHistoryItem);
    justComputed = true; 
}

