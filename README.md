My calculator to fulfill the [FreeCodeCamp challenge](https://www.freecodecamp.org/challenges/build-a-javascript-calculator).

Live website: [Calculator](https://md-fcc.gitlab.io/calculator/)

The user must click the calculator buttons. Keyboard input is not implemented.

This calculator uses the [Shunting-yard algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm). The user inputs an expression in prefix notation, then the expression is converte to postfix notation, and finally evaluated using the Shunting-yard algorithm.

I saw other examples Javascript calculators that use "eval" or hard-coded logic. By me implementing the Shunting-yard algorithm, I learned more and had to solve some interesting issues, for example the negative sign can mean subtraction or a negative number, or dealing with rounding and floating-point arithmetic.

My Javascript code is organized by having a Calculator class that implements the algorithms for parsing and evaluating expressions. This encapsulates the calculator logic, but the remaining functionality is implemented in the global namespace. (A larger project should make efforts minimize global namespace pollution.) There is also a variable expressionStack that is an array containing the user inputs.

The HTML uses onclick="inputFunction()" for all the input buttons, where inputFunction() performs some action on the current expression stack, like inputNumber or inputOperator.

